package lazocreations.lccore.bukkit.config;

import lazocreations.lccore.bukkit.LCCore;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Random;
import java.util.logging.Logger;

public class LCConfig {
    
    public static MainConfig main = null;
    public static MinigamesConfig minigames = null;
    protected static LCCore core;
    protected static File dataFolder;
    protected static Logger log;
    private static LCConfig instance = null;
    
    protected FileConfiguration config;
    protected String fileName;
    protected String resourceName;
    protected int version = 0;
    
    public static void initialize() {
        if(LCConfig.instance == null) {
            LCConfig instance = new LCConfig();
            instance.init();
            
            LCConfig.instance = instance;
        }
    }
    
    public FileConfiguration getConfig() {
        return config;
    }
    
    public void loadConfig() {
        File file = new File(dataFolder, fileName);
        
        if(file.exists()) {
            config = YamlConfiguration.loadConfiguration(file);
            
            if(config.getInt("version") != version) {
                log.warning(fileName + " seems to be old. Backuping the old one and generating new file..");
                
                int maxCount = 0;
                File backup = new File(file.getParent(), fileName + "." + new Random().nextInt(100));
                while(backup.exists()) {
                    if(maxCount++ > 5) {
                        log.severe("PLZ CLEAN UP THE OLD CONFIG BACKUPS (T.T) Disabling plugin..");
                        core.getServer().getPluginManager().disablePlugin(core);
                        return;
                    }
                    backup = new File(file.getParent(), fileName + "." + new Random().nextInt(1000));
                }
                file.renameTo(backup);
            } else {
                log.info("Loaded latest version of " + fileName);
                return;
            }
        }
        
        core.saveResource(resourceName, false);
        File newFile = new File(dataFolder, resourceName);
        
        if(newFile.exists() && newFile.isFile()) {
            newFile.renameTo(new File(newFile.getParent(), fileName));
            log.info(fileName + " has been newly generated.");
        } else {
            log.severe("Failed to generate " + fileName);
        }
    }
    
    private void init() {
        LCConfig.core = LCCore.core;
        LCConfig.log = core.getLogger();
        LCConfig.dataFolder = core.getDataFolder();
    
        if (dataFolder.mkdirs()) {
            core.getLogger().info("Plugin's data folder was created.");
        }
    
        loadConfigInstances();
    }
    
    private void loadConfigInstances() {
        main = new MainConfig();
        minigames = new MinigamesConfig();
    }
}