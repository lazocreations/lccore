package lazocreations.lccore.bukkit.config;

public class MainConfig extends LCConfig {
    
    public MainConfig() {
        this.version = 5;
        this.fileName = "config.yml";
        this.resourceName = "bukkit-config.yml";
    }
    
}
