package lazocreations.lccore.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LCWarp {
    
    private static HashMap<String, LCWarp> warps;
    private static HashMap<String, Location> destinations;
    private static Connection con;
    
    private String name;
    private String world;
    private String desti;
    private Vector min;
    private Vector max;
    
    static {
        warps = new HashMap<>();
        destinations = new HashMap<>();
    }
    
    public static LCWarp[] getWarps() {
        LCWarp[] arr = new LCWarp[warps.size()];
        return warps.values().toArray(arr);
    }
    
    public static LCWarp getWarpByName(String name) {
        return warps.get(name);
    }
    
    public static List<LCWarp> getWarpsInWorld(String world) {
        try {
            List<LCWarp> warps = new ArrayList<>();
            Statement st = con.createStatement();
            ResultSet set = st.executeQuery("SELECT name FROM warp WHERE world = '" + world + "'");
            
            while(set.next()) {
                LCWarp warp = getWarpByName(set.getString("name"));
                if(warp != null) { warps.add(warp); }
            }
            
            st.close();
            return warps;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public static boolean addWarp(LCWarp warp) {
        
        String name = warp.getName();
        
        if(getWarpByName(name) != null) { return false; }
        
        String world = warp.getWorld();
        Vector min = warp.getMinPoint();
        Vector max = warp.getMaxPoint();
        String desti = warp.getDestination();
        warps.put(name, warp);
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO warp (name, world, minX, minY, minZ, maxX, maxY, maxZ, destination) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, name); ps.setString(2, world);
            ps.setInt(3, min.getBlockX()); ps.setInt(4, min.getBlockY());
            ps.setInt(5, min.getBlockZ()); ps.setInt(6, max.getBlockX());
            ps.setInt(7, max.getBlockY()); ps.setInt(8, max.getBlockZ());
            ps.setString(9, desti);
            ps.setQueryTimeout(10);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static boolean addDestination(String name, Location loc) {
        
        if(getDestination(name) != null) { return false; }
        
        destinations.put(name, loc);
        
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO destination " +
                    "(name, world, x, y, z) VALUES (?, ?, ?, ?, ?)");
            ps.setString(1, name);
            ps.setString(2, loc.getWorld().getName());
            ps.setInt(3, loc.getBlockX());
            ps.setInt(4, loc.getBlockY());
            ps.setInt(5, loc.getBlockZ());
            ps.setQueryTimeout(10);
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public static Location getDestination(String name) {
        return destinations.get(name);
    }
    
    public static boolean removeWarp(String name) {
        
        if(getWarpByName(name) == null) {
            return false;
        }
        
        warps.remove(name);
        
        try {
            Statement st = con.createStatement();
            st.executeUpdate("DELETE FROM warp WHERE name = '" + name + "'");
            st.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public static boolean removeDestination(String name) {
        
        if(getDestination(name) == null) {
            return false;
        }
        
        destinations.remove(name);
        try {
            Statement st = con.createStatement();
            st.executeUpdate("DELETE FROM destination WHERE name = '" + name + "'");
            st.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    protected static void closeConnection() {
        try {
            if(con != null) { con.close(); }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    protected static void initData() {
    
        try {
            con = DriverManager.getConnection("jdbc:sqlite:plugins/LCCore/warps.data");
            Statement st = con.createStatement();
            ResultSet set;
            st.setQueryTimeout(30);
            st.executeUpdate("CREATE TABLE IF NOT EXISTS warp " +
                    "(name TEXT PRIMARY KEY, world TEXT, minX INTEGER, minY Integer, minZ Integer, " +
                    "maxX INTEGER, maxY INTEGER, maxZ INTEGER, destination TEXT)");
            st.executeUpdate("CREATE TABLE IF NOT EXISTS destination " +
                    "(name TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER)");
            
            set = st.executeQuery("SELECT * FROM destination");
            
            while(set.next()) {
                String name = set.getString(1);
                World world = Bukkit.getWorld(set.getString(2));
                int x = set.getInt(3);
                int y = set.getInt(4);
                int z = set.getInt(5);
                destinations.put(name, new Location(world, x, y, z));
            }
                    
            set = st.executeQuery("SELECT * FROM warp");
            
            while(set.next()) {
                String name = set.getString(1);
                String world = set.getString(2);
                Vector min = new Vector(set.getInt(3), set.getInt(4), set.getInt(5));
                Vector max = new Vector(set.getInt(6), set.getInt(7), set.getInt(8));
                String desti = set.getString(9);
                warps.put(name, new LCWarp(name, world, min, max, desti));
            }
            
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        LCCore.core.getLogger().info("Loaded " + warps.size() + " warp(s) and "
                + destinations.size() + " destination(s) from database.");
        
    }
    
    public LCWarp(String name, String world, Vector min, Vector max, String destination) {
        this.name = name;
        this.world = world;
        this.min = min;
        this.max = max;
        this.desti = destination;
    }
    
    public String getName() {
        return name;
    }
    
    public String getWorld() {
        return world;
    }
    
    public String getDestination() {
        return desti;
    }
    
    public Vector getMinPoint() {
        return min;
    }
    
    public Vector getMaxPoint() {
        return max;
    }
    
    public boolean isPlayerInWarp(Player player) {
        Location loc = player.getLocation();
        Vector vec = new Vector(loc.getX(), loc.getY(), loc.getZ());
        
        return vec.isInAABB(min, max);
    }
    
}