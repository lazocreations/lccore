package lazocreations.lccore.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class LCParty {
    
    private UUID player;
    public List<Player> members = null;
    public boolean isLeader = false;
    public boolean respond = false;
    
    public LCParty(UUID player) {
        this.player = player;
    }
    
    public synchronized void query() {
        
        Player pInstance = Bukkit.getPlayer(player);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        
        try {
            out.writeUTF("LCParty");
            out.writeUTF(player.toString());
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        pInstance.sendPluginMessage(LCCore.getPlugin(LCCore.class), "BungeeCord", stream.toByteArray());
        
    }
    
}