package lazocreations.lccore.bukkit.injector;

import au.com.mineauz.minigames.MinigamePlayer;
import au.com.mineauz.minigames.Minigames;
import au.com.mineauz.minigames.PlayerData;
import au.com.mineauz.minigames.events.EndMinigameEvent;
import au.com.mineauz.minigames.events.JoinMinigameEvent;
import au.com.mineauz.minigames.events.QuitMinigameEvent;
import au.com.mineauz.minigames.events.SpectateMinigameEvent;
import au.com.mineauz.minigames.minigame.Minigame;
import com.connorlinfoot.bountifulapi.BountifulAPI;
import lazocreations.lccore.bukkit.GameType;
import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.LCParty;
import lazocreations.lccore.bukkit.config.LCConfig;
import lazocreations.lccore.bukkit.listeners.PartyQuery;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class MinigamesInject implements GameInjector, Listener {
    
    public static MinigamesInject instance;
    
    static {
        instance = new MinigamesInject();
    }

    private PlayerData playerData = Minigames.plugin.getPlayerData();
    private HashMap<UUID, String> rejoinCache = new HashMap<>();
    private HashMap<UUID, String> spectateCache = new HashMap<>();

    @Override
    public int matchGame(Player player, boolean spectate) {
        
        List<Minigame> games = new ArrayList<>(Minigames.plugin.getMinigameData().getAllMinigames().values());
        Minigame room = null;
        int count = 0;
        
        Collections.shuffle(games);
    
        for(Minigame game : games) {
            if(game.isEnabled()) {
                
                if(spectate) {
                    switch (game.getState()) {
                        case REGENERATING:
                        case ENDED:
                            continue;
                    }
    
                    if(room == null || game.getPlayers().size() > room.getPlayers().size()) {
                        room = game;
                    }
                    
                    count++;
                }
                else {
                    switch (game.getState()) {
                        case IDLE:
                        case WAITING: {
                            if (room == null || game.getPlayers().size() > room.getPlayers().size()) {
                                room = game;
                            }
                            
                            count++;
                        }
                        break;
        
                        default:
                            continue;
                    }
                }
            }
        }
        
        if(room != null) {
            MinigamePlayer mPlayer = playerData.getMinigamePlayer(player);
            
            if(spectate) {
                playerData.spectateMinigame(mPlayer, room);
            }
            else {
                playerData.joinMinigame(mPlayer, room, false, 0.0);
            }
        }
        
        return count;
    }
    
    @SuppressWarnings("unchecked")
    @EventHandler
    public void onJoinGame(JoinMinigameEvent event) {
        
        Player player = event.getPlayer();
        Minigame game = event.getMinigame();
        FileConfiguration mgConf = LCConfig.minigames.getConfig();
        LCParty party = LCCore.getPartyManager(player.getUniqueId());
        PartyQuery query = new PartyQuery(party) {
            @Override
            public void queryRun() {
                if(party.isLeader) {
                    List<String> remainders = new ArrayList<>();
                    
                    party.members.forEach(p -> {
                        MinigamePlayer pData1 = playerData.getMinigamePlayer(p);
                        if(!(player.getUniqueId().equals(p.getUniqueId()) || game.getPlayers().contains(pData1))) {
                            if (LCCore.ingamePlayers.containsKey(p.getUniqueId())) {
                                String cmd = "/mgm join " + game.getName(false);
                                TextComponent text = new TextComponent("\u00A76>>\n\u00A76>> \u00A7e당신의 파티가 새로운 게임으로 이동했습니다!" +
                                        "\n\u00A76>> \u00A7e따라가려면 ");
                                TextComponent btn1 = new TextComponent("\u00A7c\u00A7n현재 진행중인 게임을 나가고");
                                TextComponent mid = new TextComponent("\u00A7e, ");
                                TextComponent btn2 = new TextComponent("\u00A7c\u00A7n" + cmd + "\u00A7e 을 입력하세요.");
                                TextComponent bottom = new TextComponent("\n\u00A76>>");
                                HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent("\u00A7a클릭하세요.")});
                                btn1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/leave"));
                                btn2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
                                btn1.setHoverEvent(hover);
                                btn2.setHoverEvent(hover);
                                p.spigot().sendMessage(text, btn1, mid, btn2, bottom);
                                remainders.add(p.getName());
                            }
                            else {
                                playerData.joinMinigame(playerData.getMinigamePlayer(p), game, false, 0.0);
                            }
                        }
                        p.sendMessage("\u00A7a당신의 파티가 다음 게임에 참여합니다: " + game.getName(true));
                    });
    
                    // TODO This text should be reviewed.
                    if(!remainders.isEmpty()) {
                        StringBuilder str = new StringBuilder("\u00A7e이미 다른 게임에 참여해서 따라오지 못한 파티원이 있습니다:\n\u00A7f");
                        for(String s : remainders) {
                            str = str.append(s).append(", ");
                        }
                        int last = str.lastIndexOf(",");
                        player.sendMessage(str.substring(0, last));
                    }
                }
            }
        };
        query.startQuery(10, player);
        LCCore.ingamePlayers.put(player.getUniqueId(), GameType.MINIGAMES);
        
        for(Map<?, ?> m : mgConf.getMapList("game-join.guide")) {
            HashMap<String, Object> map = (HashMap<String, Object>) m;
            
            if(map.get("game").equals(game.getName(false))) {
                player.sendMessage(" ");
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String) map.get("text")));
                player.sendMessage(" ");
            }
        }
    }

    @EventHandler
    public void onQuitGame(QuitMinigameEvent event) {
        MinigamePlayer player = event.getMinigamePlayer();
        Minigame game = player.getMinigame();
        String gameName = game.getName(false);
        boolean rejoin = game.canLateJoin();

        if(player.isInMinigame() && player.getMinigame().hasStarted()) {

            new BukkitRunnable() {
                UUID id = player.getUUID();

                @Override
                public void run() {
                    Player p = Bukkit.getPlayer(id);
                    
                    if(game.isRegenerating()) {
                        return;
                    }

                    if(p != null && p.isOnline()) {
                        sendTextAfterQuit(p, gameName, rejoin);
                    }
                    else if(rejoin){
                        rejoinCache.put(id, gameName);
                    }
                    else {
                        spectateCache.put(id, gameName);
                    }
                }
            }.runTaskLater(LCCore.core, 10L);
        }

        LCCore.ingamePlayers.remove(player.getUUID());
    }

    @EventHandler
    public void onPlayerConnect(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        UUID key = player.getUniqueId();
        String rejoin = rejoinCache.get(key);
        String spectate = spectateCache.get(key);
        String game = rejoin != null ? rejoin : spectate;

        if(game != null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    sendTextAfterQuit(player, game, rejoin != null);
                }
            }.runTaskLater(LCCore.core, 30L);

            rejoinCache.remove(key);
            spectateCache.remove(key);
        }
    }

    @EventHandler
    public void onJoinSpectate(SpectateMinigameEvent event) {

        Player player = event.getPlayer();
        MinigamePlayer mPlayer = event.getMinigamePlayer();
        Minigame gameNow = mPlayer.getMinigame();

        if(gameNow != null) {
            TextComponent text = null;
            
            if(gameNow.getSpectators().contains(mPlayer)) {
                text = new TextComponent("\u00A7c이미 게임을 관전중입니다.");
            }
            else if(gameNow.getPlayers().contains(mPlayer)) {
                text = new TextComponent("\u00A7c이미 게임에 참여했습니다.");
            }
            
            event.setCancelled(true); // TODO remove this if Minigames dev fixes this bug.
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, text);
            return;
        }
    
        LCCore.ingamePlayers.put(player.getUniqueId(), GameType.MINIGAMES);
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
                new TextComponent("\u00A7a게임 " + event.getMinigame().getName(true) + "의 관전을 시작합니다."));
    }

    @EventHandler
    public void onEndGame(EndMinigameEvent event) {
        Minigame game = event.getMinigame();
        FileConfiguration config = LCConfig.minigames.getConfig();
        int[] timing = new int[] {
                config.getInt("game-end.title.fadeIn"),
                config.getInt("game-end.title.stay"),
                config.getInt("game-end.title.fadeOut")
        };
        String[] title = new String[] {
                ChatColor.translateAlternateColorCodes('&', config.getString("game-end.title.title")),
                ChatColor.translateAlternateColorCodes('&', config.getString("game-end.title.subTitle"))
        };
        
        for(MinigamePlayer p : game.getPlayers()) {
            Player player = p.getPlayer();
            BountifulAPI.clearTitle(player);
            BountifulAPI.sendTitle(player, timing[0], timing[1], timing[2], title[0], title[1]);
        }
        
        TextComponent specText = new TextComponent();
        specText.setText("\u00A76>>\n\u00A76>> \u00A7c게임이 끝났습니다. 관전을 나가려면 \u00A7e여기를 클릭!\n\u00A76>>");
        specText.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mg quit"));
        specText.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent("\u00A7a클릭하세요.")}));
    
        for(MinigamePlayer p : game.getSpectators()) {
            p.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("\u00A76게임이 종료되었습니다."));
            
            new BukkitRunnable() {
                @Override
                public void run() {
                    p.getPlayer().spigot().sendMessage(ChatMessageType.CHAT, specText);
                }
            }.runTaskLater(LCCore.core, 100L);
        }
    }

    private void sendTextAfterQuit(Player player, String game, boolean rejoin) {
        String text, cmd;

        if(rejoin) {
            text = "\u00A76>>\n\u00A76>> 중간에 나간 게임을 다시 재개하려면 \u00A7e여기를 클릭!\n\u00A76>>";
            cmd = "/mgm join " + game;
        }
        else {
            text = "\u00A76>>\n\u00A76>> 중간에 나간 게임을 관전하려면 \u00A7e여기를 클릭!\n\u00A76>>";
            cmd = "/mgm spectate " + game;
        }

        TextComponent comp = new TextComponent(text);
        comp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
        comp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent("\u00A7a클릭하세요.")}));
        player.spigot().sendMessage(comp);
    }
    
}