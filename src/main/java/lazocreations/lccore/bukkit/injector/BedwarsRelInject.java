package lazocreations.lccore.bukkit.injector;

import io.github.bedwarsrel.events.BedwarsPlayerJoinedEvent;
import io.github.bedwarsrel.events.BedwarsPlayerLeaveEvent;
import io.github.bedwarsrel.game.Game;
import io.github.bedwarsrel.game.GameManager;
import io.github.bedwarsrel.game.GameState;
import io.github.bedwarsrel.BedwarsRel;
import lazocreations.lccore.bukkit.GameType;
import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.LCParty;
import lazocreations.lccore.bukkit.listeners.PartyQuery;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class BedwarsRelInject implements GameInjector, Listener {
    
    public static BedwarsRelInject instance;
    
    static {
        instance = new BedwarsRelInject();
    }
    
    @Override
    public int matchGame(Player player, boolean spectate) {
        GameManager gm = BedwarsRel.getInstance().getGameManager();
        List<Game> games = gm.getGames();
        Game room = null;
        int count = 0;
        
        Collections.shuffle(games);
    
        Iterator<Game> iter = games.iterator();
        
        while(iter.hasNext()) {
            Game game = iter.next();
            GameState state = game.getState();
            
            if(!game.isFull()) {
                if ((spectate && state != GameState.STOPPED) || (!spectate && state == GameState.WAITING)) {
                    if(room == null || game.getPlayerAmount() > room.getPlayerAmount()) {
                        room = game;
                    }
                    count++;
                }
            }
        }
        
        if(room != null) {
            if(spectate) {
                room.toSpectator(player);
            }
            else {
                room.playerJoins(player);
            }
        }
    
        return count;
    }
    
    @EventHandler
    public void onJoinGame(BedwarsPlayerJoinedEvent event) {
        
        Player player = event.getPlayer();
        Game game = event.getGame();
        LCParty party = LCCore.getPartyManager(player.getUniqueId());
        PartyQuery query = new PartyQuery(party) {
            @Override
            public void queryRun() {
                if(party.isLeader) {
                    List<String> remainders = new ArrayList<>();
                    party.members.forEach(p -> {
                        if(!(player.getUniqueId().equals(p.getUniqueId()) || game.isInGame(p))) {
                            if(LCCore.ingamePlayers.containsKey(p.getUniqueId())) {
                                String cmd = "/bw join " + game.getName();
                                TextComponent text = new TextComponent("\u00A76>>\n\u00A76>>\u00A7e당신의 파티가 새로운 게임으로 이동했습니다!" +
                                        "\n\u00A76>>\u00A7e따라가려면 ");
                                TextComponent btn1 = new TextComponent("\u00A7c\u00A7n현재 진행중인 게임을 나가고");
                                TextComponent mid = new TextComponent("\u00A7e, ");
                                TextComponent btn2 = new TextComponent("\u00A7c\u00A7n" + cmd + "\u00A7e 을 입력하세요.");
                                TextComponent bottom = new TextComponent("\n\u00A76>>");
                                HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent("\u00A7a클릭하세요.")});
                                btn1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/leave"));
                                btn2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, cmd));
                                btn1.setHoverEvent(hover);
                                btn2.setHoverEvent(hover);
                                p.spigot().sendMessage(text, btn1, mid, btn2, bottom);
                                remainders.add(p.getName());
                            }
                            else {
                                game.playerJoins(p);
                            }
                        }
                        p.sendMessage("\u00A7a당신의 파티가 다음 게임에 참여합니다: " + game.getName());
                    });
                    
                    // TODO This text should be reviewed.
                    if(!remainders.isEmpty()) {
                        StringBuilder str = new StringBuilder("\u00A7e이미 다른 게임에 참여해서 따라오지 못한 파티원이 있습니다:\n\u00A7f");
                        for(String s : remainders) {
                            str = str.append(s).append(", ");
                        }
                        int last = str.lastIndexOf(",");
                        player.sendMessage(str.substring(0, last));
                    }
                }
            }
        };
        query.startQuery(10, player);

        LCCore.ingamePlayers.put(player.getUniqueId(), GameType.BEDWARS_REL);

    }

    @EventHandler
    public void onQuitGame(BedwarsPlayerLeaveEvent event) {
        LCCore.ingamePlayers.remove(event.getPlayer().getUniqueId());
    }
    
}