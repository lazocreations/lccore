package lazocreations.lccore.bukkit.injector;

import lazocreations.lccore.bukkit.LCCore;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public interface GameInjector {
    
    /**
     * Randomly match a game for the player
     * @param player the target
     * @param spectate whether the player wants to spectate or join
     * @return the amount of games online. If this returns greater than 0,
     * the player should have joined the room already.
     */
    int matchGame(Player player, boolean spectate);
    
}