package lazocreations.lccore.bukkit;

import lazocreations.lccore.bukkit.commands.*;
import lazocreations.lccore.bukkit.config.LCConfig;
import lazocreations.lccore.bukkit.injector.BedwarsRelInject;
import lazocreations.lccore.bukkit.injector.MinigamesInject;
import lazocreations.lccore.bukkit.listeners.BungeeListener;
import lazocreations.lccore.bukkit.listeners.LuckPermsListener;
import lazocreations.lccore.bukkit.listeners.MinechatListener;
import lazocreations.lccore.bukkit.listeners.PlayerListener;
import me.lucko.luckperms.api.LuckPermsApi;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.Messenger;

import java.util.HashMap;
import java.util.UUID;

public class LCCore extends JavaPlugin {
    
    public static LCCore core;
    public static HashMap<UUID, LCParty> partyMap = new HashMap<>();
    public static HashMap<UUID, GameType> ingamePlayers = new HashMap<>(); // contains spectators

    public static LCParty getPartyManager(UUID player) {
        return partyMap.computeIfAbsent(player, LCParty::new);
    }

    public BedwarsRelInject bedwarsInject;
    public MinigamesInject minigamesInject;
    public LCConfig config;
    
    @Override
    public void onEnable() {
        
        LCCore.core = JavaPlugin.getPlugin(LCCore.class);
        
        PluginManager plugin = getServer().getPluginManager();
        
        loadConfigFiles();
        loadGames(plugin);
        LCWarp.initData();
        
        // Register event listeners
        ServicesManager serv = Bukkit.getServicesManager();
        plugin.registerEvents(new PlayerListener(), this);
        if(plugin.getPlugin("MineChatAPI") != null) {
            plugin.registerEvents(new MinechatListener(this), this);
        }
        
        // Register plugin messages
        Messenger msg = getServer().getMessenger();
        msg.registerIncomingPluginChannel(this, "LCParty", new BungeeListener());
        msg.registerOutgoingPluginChannel(this, "BungeeCord");
        
        // Register commands
        getCommand("lcwarp").setExecutor(new LCWarpCommand());
        getCommand("autojoin").setExecutor(new AutoMatchCommand());
        getCommand("autospectate").setExecutor(new AutoMatchCommand());
        getCommand("lobby").setExecutor(new LobbyCommand());
        getCommand("hub").setExecutor(new HubCommand());
        getCommand("lccore").setExecutor(new MainCommand());
    
        LuckPermsListener listener = new LuckPermsListener(this);
        
        if(serv.isProvidedFor(LuckPermsApi.class)) {
            LuckPermsApi api = serv.getRegistration(LuckPermsApi.class).getProvider();
            listener.registerLuckPermsListener(api);
        }
    
        listener.clearOP(getServer().getOperators());
        plugin.registerEvents(listener, this);
        getLogger().info("Due to security terms, OP permission is permanently blocked by LCCore. If you need it, type /op in console every time you join server.");
        
    }
    
    private void loadGames(PluginManager man) {
        if(getConfig().getBoolean("game-injects.BedwarsRel") && man.getPlugin("BedwarsRel") != null) {
            bedwarsInject = BedwarsRelInject.instance;
            man.registerEvents(bedwarsInject, this);
        }
        
        if(getConfig().getBoolean("game-injects.Minigames") && man.getPlugin("Minigames") != null) {
            minigamesInject = MinigamesInject.instance;
            man.registerEvents(minigamesInject, this);
        }
    }
    
    private void loadConfigFiles() {
        LCConfig.initialize();
        LCConfig.main.loadConfig();
        LCConfig.minigames.loadConfig();
    }
    
    @Override
    public void onDisable() {
        LCWarp.closeConnection();
    }
    
}