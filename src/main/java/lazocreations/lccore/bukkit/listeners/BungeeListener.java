package lazocreations.lccore.bukkit.listeners;

import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.LCParty;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BungeeListener implements PluginMessageListener {
    
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        
        DataInputStream input = new DataInputStream(new ByteArrayInputStream(message));
        
        if(!channel.equals("LCParty"))
            return;
        
        try {
            
            Player p = Bukkit.getPlayer(UUID.fromString(input.readUTF()));
            LCParty party = LCCore.getPartyManager(p.getUniqueId());
            String isInParty = input.readUTF();
            
            if(party == null)
                return;
            
            if(isInParty.equals("#NotInParty")) {
                
                party.isLeader = false;
                
            } else {
                
                party.isLeader = Boolean.valueOf(isInParty);
                List<Player> players = new ArrayList<>();
                
                while (true) {
                    String str = input.readUTF();
        
                    if (!str.equals("#END")) {
                        Player p1 = Bukkit.getPlayer(str);
                        if (p1 != null) players.add(p1);
                    } else
                        break;
                }
                // TODO Add list of players who aren't in leader's server
                party.members = players;
                
            }
            
            party.respond = true;
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
    }
    
}
