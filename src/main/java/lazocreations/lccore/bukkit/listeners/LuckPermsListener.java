package lazocreations.lccore.bukkit.listeners;

import lazocreations.lccore.bukkit.LCCore;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.PermissionHolder;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.event.EventBus;
import me.lucko.luckperms.api.event.node.NodeAddEvent;
import me.lucko.luckperms.api.event.node.NodeMutateEvent;
import me.lucko.luckperms.api.event.node.NodeRemoveEvent;
import me.lucko.luckperms.exceptions.ObjectAlreadyHasException;
import me.lucko.luckperms.exceptions.ObjectLacksException;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Set;

public class LuckPermsListener implements Listener {
    
    private LCCore core;
    
    public LuckPermsListener(LCCore core) {
        this.core = core;
    }
    
    public void registerLuckPermsListener(LuckPermsApi api) {
        EventBus eventBus = api.getEventBus();
        eventBus.subscribe(NodeAddEvent.class, this::onNodeAdd);
    }
    
    private void onNodeAdd(NodeAddEvent event) {
    
        LuckPermsApi api = event.getApi();
        PermissionHolder holder = event.getTarget();
        Node node = event.getNode();
    
        Node negOp =  api.getNodeFactory().newBuilder("minecraft.command.op").setNegated(true).build();
        boolean negOpExists;
        
        switch(holder.hasPermission(negOp)) {
            case FALSE:
                negOpExists = true;
                break;
            default:
                negOpExists = false;
        }
        
        if(node.getPermission().equals("minecraft.command.op") && !node.isNegated()) {
            try {
                holder.unsetPermission(node);
            } catch (ObjectLacksException e) {
                e.printStackTrace();
            }
            api.getLogger().info("LCCore blocked OP permission that is given to someone.");
        }
        else if(node.getPermission().contains("*") && !node.isNegated() && !negOpExists) {
            try {
                if(event.isUser()) {
                    User user = (User) holder;
                    user.setPermission(negOp);
                    api.getStorage().saveUser(user);
                }
                else {
                    holder.setPermission(negOp);
                }
            } catch (ObjectAlreadyHasException e) {
                e.printStackTrace();
            }
            api.getLogger().info("LCCore detected wildcard permission that is given to someone.");
        }
        
    }
    
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        
        String cmd = event.getMessage();
        
        if(cmd.startsWith("/op")) {
            Player player = event.getPlayer();
            
            event.setCancelled(true);
            player.sendMessage("\u00A7cYou don't have permission to do that.");
            core.getLogger().info(player.getName() + " tried to use /op in game.");
        }
        
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        Player player = event.getPlayer();
        
        if(player.isOp()) {
            player.setOp(false);
            deopInform(player);
        }
        
    }
    
    public void clearOP(Set<OfflinePlayer> players) {
        
        for(OfflinePlayer p : players) {
            if(p.isOnline()) {
                p.setOp(false);
                deopInform(p.getPlayer());
            }
        }
        
    }
    
    private void deopInform(Player player) {
        core.getLogger().info("Demoted " + player.getName() + " from op.");
        player.sendMessage("\u00A7eSecurity system has demoted you from op.");
    }
    
}