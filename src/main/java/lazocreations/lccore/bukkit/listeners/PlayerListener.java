package lazocreations.lccore.bukkit.listeners;

import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.LCWarp;
import lazocreations.lccore.bukkit.events.LCWarpEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.Iterator;
import java.util.List;

public class PlayerListener implements Listener {
    
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
    
        FileConfiguration config = LCCore.core.getConfig();
        
        if(config.getBoolean("minigames.loadout.disable-command")
                && event.getMessage().toLowerCase().startsWith("/mg loadout"))
        {
            event.setCancelled(true);
        }
        
    }
    
    @EventHandler
    public void onPlayerWarp(PlayerInteractEvent event) {
    
        Block block = event.getClickedBlock();
        
        if(!(event.getAction() == Action.PHYSICAL && block != null &&
                block.getType().toString().endsWith("PLATE")))
        { return; }

        List<LCWarp> warps = LCWarp.getWarpsInWorld(block.getWorld().getName());
        
        if(warps == null) { return; }
        
        Iterator<LCWarp> iter = warps.iterator();
        final Player player = event.getPlayer();
        Location loc = block.getLocation();
        Vector centre = new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        
        while(iter.hasNext()) {
            
            LCWarp warp = iter.next();
            
            if(centre.isInAABB(warp.getMinPoint(), warp.getMaxPoint())) {
                
                LCWarpEvent warpEvent = new LCWarpEvent(player, warp);
                Bukkit.getServer().getPluginManager().callEvent(warpEvent);
                
                if(warpEvent.isCancelled()) { return; }
                
                String desti = warp.getDestination();
                
                if(desti != null) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Location dest = LCWarp.getDestination(desti);
                            player.teleport(dest, PlayerTeleportEvent.TeleportCause.PLUGIN);
                        }
                    }.runTaskLater(JavaPlugin.getPlugin(LCCore.class), 3L);
                }
                
            }
    
        }
        
    }
    
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        LCCore.partyMap.remove(event.getPlayer().getUniqueId());
    }
    
}