package lazocreations.lccore.bukkit.listeners;

import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.LCParty;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class PartyQuery {
    
    private LCParty party;
    
    public PartyQuery(LCParty party) {
        this.party = party;
    }
    
    /**
     * Start the party query with given function that you defined by implementing queryRun().
     * @param maxAttempt Maximum attempts (numeric)
     * @param player to inform if bungeecord doesn't response (nullable)
     */
    public void startQuery(int maxAttempt, Player player) {
        party.query();
        
        new BukkitRunnable() {
            int attempt = 0;
            
            @Override
            public void run() {
                if(party.respond) {
                    queryRun();
                    party.respond = false;
                    cancel();
                }
                else if(attempt++ > maxAttempt) {
                    if(player != null) {
                        player.sendMessage("\u00A7cCan't retrieve response from bungeecord.");
                    }
                    cancel();
                }
            }
        }.runTaskTimer(LCCore.core, 5L, 5L);
    }
    
    /**
     * Define the function you want to do with querying party
     * from bungeecord messaging by implementing this.
     */
    public abstract void queryRun();
    
}