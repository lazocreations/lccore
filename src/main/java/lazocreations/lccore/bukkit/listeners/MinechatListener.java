package lazocreations.lccore.bukkit.listeners;

import lazocreations.lccore.bukkit.LCCore;
import me.xmx.minechatapi.ClientManager;
import me.xmx.minechatapi.events.EventMineChatClientJoin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class MinechatListener implements Listener {
    
    private String prefix = null;
    private String greeting = null;
    
    public MinechatListener(LCCore core) {
        String pref = core.getConfig().getString("minechat.prefix");
        String greet = core.getConfig().getString("minechat.greeting");
        
        greeting = ChatColor.translateAlternateColorCodes('&', greet);
        prefix = ChatColor.translateAlternateColorCodes('&', pref);
    }
    
    @EventHandler
    public void onJoin(EventMineChatClientJoin event) {
        
        Player player = event.getPlayer();
        
        if(greeting != null)
            player.getServer().broadcastMessage(greeting.replace("%1", player.getDisplayName()).replace("%2", event.getConnection().getPhoneType().toString()));
        
    }
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        
        if(prefix == null)
            return;
        
        Player player = event.getPlayer();
        
        if(ClientManager.usingMineChat(player)) {
            event.setMessage(new StringBuilder(prefix).append(" ").append(event.getMessage()).toString());
        }
        
    }
    
}
