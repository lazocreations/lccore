package lazocreations.lccore.bukkit.commands;

import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.injector.GameInjector;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class AutoMatchCommand implements CommandExecutor {
    
    private LCCore core = LCCore.core;
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        boolean spectateMode = false;
        
        if(command.getName().equals("autospectate")) {
            spectateMode = true;
        }
        else if(!command.getName().equals("autojoin")) {
            return true;
        }
        
        if(args.length < 1) {
            if(spectateMode) {
                sender.sendMessage("/autojoin (game) - Join the most crowded game at this time.");
            }
            else {
                sender.sendMessage("/autospectate (game) - Spectate the most crowded game at this time.");
            }
        }
        
        else if(sender instanceof Player) {
            Player player = (Player) sender;
            GameInjector injector = null;
            
            switch(args[0].toLowerCase()) {
                case "bedwarsrel":
                    injector = core.bedwarsInject;
                    break;
                    
                case "minigames":
                    injector = core.minigamesInject;
                    break;
            }
            
            if(LCCore.ingamePlayers.containsKey(player.getUniqueId())) {
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("\u00A7e이미 게임에 참여했습니다."));
                return true;
            }
            
            if(injector != null) {
                
                if(injector.matchGame(player, spectateMode) > 0) {
                    sender.sendMessage("\u00A7aSuccessfully matched a suitable game!");
                }
                else {
                    sender.sendMessage("\u00A7cNo game is available yet. Please wait and try again");
                }
                
            } else {
                sender.sendMessage("\u00A7cThis game does not exist: " + args[0]);
            }
        }
        
        else {
            sender.sendMessage("Console can not use this command.");
        }
        
        return true;
        
    }
    
}
