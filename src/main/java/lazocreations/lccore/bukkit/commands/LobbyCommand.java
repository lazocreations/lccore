package lazocreations.lccore.bukkit.commands;

import au.com.mineauz.minigames.MinigamePlayer;
import au.com.mineauz.minigames.Minigames;
import au.com.mineauz.minigames.PlayerData;
import io.github.bedwarsrel.BedwarsRel;
import io.github.bedwarsrel.game.Game;
import lazocreations.lccore.bukkit.GameType;
import lazocreations.lccore.bukkit.LCCore;
import lazocreations.lccore.bukkit.config.LCConfig;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class LobbyCommand extends CooldownCmd implements CommandExecutor {
    
    private FileConfiguration config = LCCore.core.getConfig();
    private Location spawn;
    private boolean hub;
    
    public LobbyCommand() {
        super("lobby.delay");
        this.hub = config.getBoolean("hub.enable");
        loadSpawn();
    }
    
    private void loadSpawn() {
        World world = Bukkit.getWorld(config.getString("lobby.world"));
    
        if(world != null) {
            spawn = new Location(world, config.getDouble("lobby.x"), config.getDouble("lobby.y"), config.getDouble("lobby.z"));
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        final Player player = getPlayerBySender(sender);
        UUID id = player.getUniqueId();
        FileConfiguration config = LCConfig.main.getConfig();
        
        if(player == null) {
            return true;
        }
        
        if(!config.getBoolean("lobby.enable")) {
            player.sendMessage("\u00A7c이 서버는 로비를 사용할 수 없습니다.");
            
            if(config.getBoolean("hub.enable")) {
                player.sendMessage("\u00A7e/hub \u00A7f를 입력해 광장 서버로 나갈 수 있습니다.");
            }
            return true;
        }
    
        if(!sender.hasPermission("lccore.lobby")) {
            sender.sendMessage("\u00A7cYou don't have permission.");
            return true;
        }
        
        if(args.length > 0) {
            String arg0 = args[0].toLowerCase();
            
            if(arg0.equals("help")) {
                player.sendMessage(new String[] {
                        "/lobby - 로비로 이동합니다.",
                        "/lobby set - 로비 위치를 지정합니다."
                });
            }
            else if(arg0.equals("set")) {
    
                if(!sender.hasPermission("lccore.lobby.set")) {
                    sender.sendMessage("\u00A7cYou don't have permission.");
                    return true;
                }
                
                Location loc = player.getLocation();
                config.set("lobby.world", player.getWorld().getName());
                config.set("lobby.x", Double.parseDouble(String.format("%.3f", loc.getX())));
                config.set("lobby.y", Double.parseDouble(String.format("%.3f", loc.getY())));
                config.set("lobby.z", Double.parseDouble(String.format("%.3f", loc.getZ())));
                
                try {
                    config.save(new File(LCCore.core.getDataFolder(), "config.yml"));
                    LCCore.core.reloadConfig();
                    loadSpawn();
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
    
                player.sendMessage("\u00A7a로비가 현 위치로 설정되었습니다.");
                return true;
                
            }
            else {
                return false;
            }
            return true;
        }
        
        if(isUnderCooldown(player)) {
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("\u00A7e잠시 후 다시 입력하세요."));
            return true;
        }
        
        else {
            GameType type = LCCore.ingamePlayers.get(id);
            
            if(type == null) {
                if(spawn == null) {
                    player.sendMessage("\u00A7c스폰 위치가 정의되지 않았거나 오류가 발생했습니다.");
                } else {
                    player.teleport(spawn);
                }
                
                if(hub) {
                    new BukkitRunnable() {
                        int count = 0;
                        
                        @Override
                        public void run() {
                            if(player == null || count++ >= config.getInt("lobby.hub-guide.duration") * 2) {
                                cancel();
                            }
                            
                            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(
                                    ChatColor.translateAlternateColorCodes('&', config.getString("lobby.hub-guide.text"))
                            ));
                        }
                    }.runTaskTimer(LCCore.core, 10L, 10L);
                }
                
                return true;
            }
    
            switch(type) {
                case MINIGAMES:
                    PlayerData pData = Minigames.plugin.getPlayerData();
                    MinigamePlayer p = pData.getMinigamePlayer(id);
                    if(p != null) {
                        pData.quitMinigame(p, false);
                    }
                    break;
                
                case BEDWARS_REL:
                    Game game = BedwarsRel.getInstance().getGameManager().getGameOfPlayer(player);
                    if(game != null) {
                        game.playerLeave(player, false);
                    }
                    break;
        
                default:
                    player.sendMessage("This game is not supported by /leave command of LCCore.");
            }
            
            executeCooldown(player);
            return true;
        }
    }
}