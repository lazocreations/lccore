package lazocreations.lccore.bukkit.commands;

import lazocreations.lccore.bukkit.config.LCConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCommand implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(args.length < 1) {
            sender.sendMessage("Type \'/lccore reload\' to reload config.");
            return false;
        }
        
        String arg0 = args[0].toLowerCase();
        
        if(arg0.equals("reload")) {
            if(!sender.hasPermission("lccore.reload")) {
                sender.sendMessage("\u00A7cYou don't have permission.");
                return true;
            }
            
            reloadConfigs();
            sender.sendMessage("\u00A7aReloaded configurations.");
            return true;
        }
        
        return false;
        
    }
    
    private void reloadConfigs() {
        LCConfig.main.loadConfig();
        LCConfig.minigames.loadConfig();
    }
}
