package lazocreations.lccore.bukkit.commands;

import lazocreations.lccore.bukkit.LCCore;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CooldownCmd {
    
    private FileConfiguration config = LCCore.core.getConfig();
    private List<UUID> cooldown = new ArrayList<>();
    private int delay;
    
    public CooldownCmd(String delayPath) {
        delay = config.getInt(delayPath);
    }
    
    protected Player getPlayerBySender(CommandSender sender) {
        if(sender instanceof Player) {
            return (Player) sender;
        }
        return null;
    }
    
    protected void executeCooldown(Player player) {
        cooldown.add(player.getUniqueId());
        
        new BukkitRunnable() {
            @Override
            public void run() {
                cooldown.remove(player.getUniqueId());
            }
        }.runTaskLater(LCCore.core, 20L * delay);
    }
    
    protected boolean isUnderCooldown(Player player) {
        return cooldown.contains(player.getUniqueId());
    }
    
}