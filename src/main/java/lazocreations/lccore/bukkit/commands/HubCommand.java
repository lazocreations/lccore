package lazocreations.lccore.bukkit.commands;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lazocreations.lccore.bukkit.LCCore;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class HubCommand extends CooldownCmd implements CommandExecutor {
    
    private String lobbyServer = null;
    
    public HubCommand() {
        super("hub.delay");
        
        FileConfiguration config = LCCore.core.getConfig();
        if(config.getBoolean("hub.enable")) {
            lobbyServer = config.getString("hub.server-name");
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = getPlayerBySender(sender);
        
        if(player == null || lobbyServer == null)
            return true;
    
        if(!sender.hasPermission("lccore.hub")) {
            sender.sendMessage("\u00A7cYou don't have permission.");
            return true;
        }
        
        if(isUnderCooldown(player)) {
            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("\u00A7c잠시 후 다시 입력하세요."));
            return true;
        }
        
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(lobbyServer);
        player.sendPluginMessage(LCCore.core, "BungeeCord", out.toByteArray());
        
        executeCooldown(player);
        return true;
    }
}
