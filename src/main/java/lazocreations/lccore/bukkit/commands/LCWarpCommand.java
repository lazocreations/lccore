package lazocreations.lccore.bukkit.commands;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import lazocreations.lccore.bukkit.LCWarp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class LCWarpCommand implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(!command.getName().equals("lcwarp") || !(sender instanceof Player)) {
            return false;
        }
        
        Player player = (Player) sender;
        
        if(!sender.hasPermission("lccore.warp")) {
            sender.sendMessage("\u00A7cYou don't have permission.");
            return true;
        }
        
        if(args.length < 1 || args[0].equalsIgnoreCase("help")) {
            sender.sendMessage(new String[] {
                    "/lcwarp create (name) [destination] - Make a warp with WorldEdit-selected cuboid area.\n",
                    "/lcwarp remove (name) - Remove the warp with given name.\n",
                    "/lcwarp destination create (name) - Make a destination at the current position.\n",
                    "/lcwarp destination remove (name) - Remove the destination with given name."
            });
            return true;
        }
        
        else if(args.length > 1) {
            
            String arg0 = args[0].toLowerCase();
            String arg1 = args[1].toLowerCase();
            
            if(arg0.equals("destination") && args.length > 2) {
    
                switch (arg1) {
                    case "create": {
            
                        boolean success = LCWarp.addDestination(args[2], player.getLocation());
            
                        if (success)
                            player.sendMessage("Destination '" + args[2] + "' has been added.");
            
                        else
                            player.sendMessage("Failed to add destination.");
            
                        break;
                    }
                    case "remove": {
            
                        boolean success = LCWarp.removeDestination(args[2]);
            
                        if (success)
                            player.sendMessage("Removed destination '" + args[2] + "'.");
            
                        else
                            player.sendMessage("Failed to remove destination.");
            
                        break;
                    }
                    default:
                        return false;
                }
                
            }
            
            else if(arg0.equals("create")) {
    
                WorldEditPlugin we = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
                
                if(we == null) {
                    player.sendMessage("It seems WorldEdit is not loaded!");
                    return true;
                }
                
                Selection sel = we.getSelection(player);
                
                if(sel == null) {
                    player.sendMessage("Make sure to have an area selected using WorldEdit.");
                    return true;
                }
                
                Location min = sel.getMinimumPoint();
                Location max = sel.getMaximumPoint();
                String desti = null;
                
                if(args.length > 2) {
                    desti = args[2];
                }
                
                boolean success = LCWarp.addWarp(new LCWarp(arg1, player.getWorld().getName(),
                        new Vector(min.getBlockX(), min.getBlockY(), min.getBlockZ()),
                        new Vector(max.getBlockX(), max.getBlockY(), max.getBlockZ()), desti));
                
                if(success)
                    player.sendMessage("Warp '" + arg1 + "' has been created!");
                
                else
                    player.sendMessage("Failed to create warp.");
                
            }
            
            else if(arg0.equals("remove")) {
                
                boolean success = LCWarp.removeWarp(arg1);
                
                if(success)
                    player.sendMessage("Removed warp '" + arg1 + "'.");
                
                else
                    player.sendMessage("Failed to remove warp.");
                
            }
            
            else
                return false;
            
        }
        
        return false;
    }
    
}
