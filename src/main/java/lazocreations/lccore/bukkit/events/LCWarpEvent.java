package lazocreations.lccore.bukkit.events;

import lazocreations.lccore.bukkit.LCWarp;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LCWarpEvent extends Event implements Cancellable {
    
    private static final HandlerList handlers = new HandlerList();
    
    private Player player;
    private LCWarp warp;
    private boolean cancelled;
    
    public LCWarpEvent(Player player, LCWarp warp) {
        this.player = player;
        this.warp = warp;
        this.cancelled = false;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public LCWarp getWarp() {
        return warp;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    @Override
    public boolean isCancelled() {
        return cancelled;
    }
    
    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
    
}