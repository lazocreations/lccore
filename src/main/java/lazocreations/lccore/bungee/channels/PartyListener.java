package lazocreations.lccore.bungee.channels;

import de.simonsator.partyandfriends.api.pafplayers.OnlinePAFPlayer;
import de.simonsator.partyandfriends.api.pafplayers.PAFPlayerManager;
import de.simonsator.partyandfriends.api.party.PartyAPI;
import de.simonsator.partyandfriends.api.party.PlayerParty;
import lazocreations.lccore.bungee.LCCoreBungee;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.*;
import java.util.List;
import java.util.UUID;

public class PartyListener implements Listener {
    
    private LCCoreBungee coreB;
    
    public PartyListener(LCCoreBungee c) {
        this.coreB = c;
        ProxyServer.getInstance().registerChannel("LCParty");
    }
    
    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) {
        
        if (!event.getTag().equalsIgnoreCase("BungeeCord"))
            return;
        
        DataInputStream input = new DataInputStream(new ByteArrayInputStream(event.getData()));
        
        try {
            String channel = input.readUTF();
            
            if(channel.equals("LCParty")) {
                
                ProxiedPlayer player = coreB.getProxy().getPlayer(UUID.fromString(input.readUTF()));
                
                if(player == null) {
                    return;
                }
                
                OnlinePAFPlayer pafPlayer = PAFPlayerManager.getInstance().getPlayer(player);
                PlayerParty party = PartyAPI.getParty(pafPlayer);
                String[] message;
                
                if(party == null) {
                    message = new String[] {"#NotInParty"};
                }
                
                else {
                    List<OnlinePAFPlayer> players = party.getAllPlayers();
                    String[] str = new String[players.size() + 1];
                    str[0] = String.valueOf(party.isLeader(pafPlayer));
                    for(int i=0; i<str.length - 1; i++) {
                        str[i + 1] = players.get(i).getName();
                    }
                    message = str;
                }
                
                sendToBukkit(message, player);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    
    }
    
    public void sendToBukkit(String[] message, ProxiedPlayer player) {
        
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        
        try {
            out.writeUTF(player.getUniqueId().toString());
            for (String msg : message) { out.writeUTF(msg); }
            out.writeUTF("#END");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        player.getServer().getInfo().sendData("LCParty", stream.toByteArray());
        
    }
    
}
