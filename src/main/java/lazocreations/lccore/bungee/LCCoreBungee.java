package lazocreations.lccore.bungee;

import lazocreations.lccore.bungee.channels.PartyListener;
import net.md_5.bungee.api.plugin.Plugin;

public class LCCoreBungee extends Plugin {
    
    @Override
    public void onEnable() {
    
        getProxy().getPluginManager().registerListener(this, new PartyListener(this));
        
    }
    
}